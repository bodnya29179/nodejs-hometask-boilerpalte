const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// Get all fighters
router.get('/', responseMiddleware, function (req, res) {
    const allFighters = FighterService.getAllFighters();

    if (allFighters) {
        res.status(200).send(allFighters);
    } else {
        res.status(400).send({
            "error": true,
            "message": "Fighters are not found!"
        });
    }
});

// Get fighter by ID
router.get('/:id', responseMiddleware, function (req, res) {
    const fighter = FighterService.getFighterById(req.params.id);

    if (fighter) {
        res.status(200).send(fighter);
    } else {
        res.status(404).send({
            "error": true,
            "message": "Fighter is not found!"
        });
    }
});

// Create a new fighter
router.post('/', responseMiddleware, createFighterValid, function (req, res) {
    const fighter = {
        name: req.body.name,
        health: req.body.health,
        power: req.body.power,
        defense: req.body.defense,
    };

    if (!fighter.health) {
        fighter.health = 100;
    }
    const result = FighterService.createFighter(fighter);
    if (result) {
        res.status(200).send(result);
    }
});

// Update fighter's data 
router.put('/:id', responseMiddleware, updateFighterValid, function (req, res) {
    let fighter = FighterService.getFighterById(req.params.id);
    for (let key in req.body) {
        if (fighter[key]) {
            fighter[key] = req.body[key];
        }
    }

    const result = FighterService.updateFighter(fighter);
    if (result) {
        res.status(200).send(result);
    }
});

// Delete the fighter
router.delete('/:id', function(req, res) {
    const result = FighterService.deleteFighter(req.params.id);

    if (result) {
        res.status(200).send({
            "error": false,
            "message": "Fighter is deleted."
        });
    } else {
        res.status(404).send({
            "error": true,
            "message": "Fighter is not found."
        });
    }
});

module.exports = router;