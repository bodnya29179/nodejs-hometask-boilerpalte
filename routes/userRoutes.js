const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// Get all users
router.get('/', responseMiddleware, function (req, res) {
    const allUsers = UserService.getAllUsers();

    if (allUsers) {
        res.status(200).send(allUsers);
    } else {
        res.status(400).send({
            "error": true,
            "message": "Bad GET request!"
        });
    }
});

// Get user by ID
router.get('/:id', responseMiddleware, function (req, res) {
    const user = UserService.getUserById(req.params.id);

    if (user) {
        res.status(200).send(user);
    } else {
        res.status(404).send({
            "error": true,
            "message": "User is not found!"
        });
    }
});

// Create a new user
router.post('/', responseMiddleware, createUserValid, function (req, res) {
    const user = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        phoneNumber: req.body.phoneNumber,
        password: req.body.password,
    };

    const result = UserService.createUser(user);
    if (result) {
        res.status(200).send(result);
    }
});

// Update user's data 
router.put('/:id', responseMiddleware, updateUserValid, function (req, res) {
    let user = UserService.getUserById(req.params.id);
    for (let key in req.body) {
        if (user[key]) {
            user[key] = req.body[key];
        }
    }

    const result = UserService.updateUser(user);
    if (result) {
        res.status(200).send(result);
    }
});

// Delete the user
router.delete('/:id', responseMiddleware, function(req, res) {
    const result = UserService.deleteUser(req.params.id);

    if (result) {
        res.status(200).send({
            "error": false,
            "message": "User is deleted."
        });
    } else {
        res.status(404).send({
            "error": true,
            "message": "User is not found!"
        });
    }
});


module.exports = router;