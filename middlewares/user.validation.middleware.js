const { user } = require('../models/user');
const UserService = require('../services/userService');

// Email address validation
const regExpEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

// Phone number validation
const regExpPhoneNumber = /^\+?([3][8][0])\)?([0-9]{9})$/;

// Get all user keys from model
const userKeys = Object.keys(user);

// User creating validation
const createUserValid = (req, res, next) => {
    const fields = Object.keys(req.body);
    const { id, firstName, lastName, email, phoneNumber, password } = req.body;

    if (
        firstName
        && lastName
        && regExpEmail.test(email) 
        && email.includes('@gmail.com')
        && phoneNumber
        && regExpPhoneNumber.test(phoneNumber) 
        && !id 
        && fields.length == 5 
        && password.length >= 3
        && fields.every(key => userKeys.indexOf(key) >= 0)
    ) {
        next();
    } else if (
        id
        || !firstName
        || !lastName
        || !regExpPhoneNumber.test(phoneNumber)
        || !phoneNumber 
        || !regExpEmail.test(email) 
        || !email.includes('@gmail.com')
        || password.length < 3
        || !fields.every(key => userKeys.indexOf(key) >= 0)
    ) {
        res.status(400).send({
            "error": true,
            "message": "Validation or data processing error!"
        });
    } else {
        res.status(404).send({
            "error": true,
            "message": "Not found!"
        });
    }
}

// User updating validation
const updateUserValid = (req, res, next) => {
    const fields = Object.keys(req.body);

    // Check if there are some extra properties or ID in req.body or incorrect ID as URL param
    if (
        !fields.every(key => userKeys.indexOf(key) >= 0) 
        || req.body.id
        || !UserService.getUserById(req.params.id)
    ) {
        res.status(400).send({
            "error": true,
            "message": "Validation or data processing error!"
        });
    } else {
        // Check fields which we want to update
        for (let key in req.body) {
            if (key === 'email' && (!req.body[key].includes('@gmail.com') || !regExpEmail.test(req.body[key]))) {
                res.status(400).send({
                    "error": true,
                    "message": "Validation or data processing error!"
                });
                break;
            }
            if (key === 'password' && req.body[key].length < 3) {
                res.status(400).send({
                    "error": true,
                    "message": "Validation or data processing error!"
                });
                break;
            }
            if (key === 'phoneNumber' && !regExpPhoneNumber.test(req.body[key])) {
                res.status(400).send({
                    "error": true,
                    "message": "Validation or data processing error!"
                });
                break;
            } 
        }

        // All is correct, let's continue
        next();
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;