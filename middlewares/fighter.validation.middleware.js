const { fighter } = require('../models/fighter');

// Get all fighter keys from model
const fighterKeys = Object.keys(fighter);

// Fighter creating validation
const createFighterValid = (req, res, next) => {
    const fields = Object.keys(req.body);
    const { id, name, health = 100, power, defense } = req.body;

    if (
        name
        && !id
        && defense >= 1
        && defense <= 10
        && power > 0
        && power < 100
        && !isNaN(power)
        && !isNaN(defense)
        && !isNaN(health)
        && health > 0
        && fields.every(key => fighterKeys.indexOf(key) >= 0)
    ) {
        next();
    } else if (
       !name
       || id
       || power <= 0
       || power >= 100
       || isNaN(power)
       || isNaN(defense)
       || isNaN(health)
       || health <= 0
       || defense < 1
       || defense > 10
       || !fields.every(key => fighterKeys.indexOf(key) >= 0)
    ) {
        res.status(400).send({
            "error": true,
            "message": "Validation or data processing error!"
        });
    } else {
        res.status(404).send({
            "error": true,
            "message": "Not found!"
        });
    }
}

// Fighter updating validation
const updateFighterValid = (req, res, next) => {
    const fields = Object.keys(req.body);

    // Check if there are some extra properties or ID in req.body
    if (
        !fields.every(key => fighterKeys.indexOf(key) >= 0)
        || req.body.id
    ) {
        res.status(400).send({
            "error": true,
            "message": "Validation or data processing error!"
        });
    } else {
        // Check fields which we want to update
        for (let key in req.body) {
            if (key === 'health' && req.body[key] < 0) {
                res.status(400).send({
                    "error": true,
                    "message": "Validation or data processing error!"
                });
                break;
            }
            if (key === 'defense' && (req.body[key] < 1 || req.body[key] > 10 || isNaN(req.body[key]))) {
                res.status(400).send({
                    "error": true,
                    "message": "Validation or data processing error!"
                });
                break;
            }
            if (key === 'power' && (req.body[key] <= 0 || req.body[key] >= 100 || isNaN(req.body[key]))) {
                res.status(400).send({
                    "error": true,
                    "message": "Validation or data processing error!"
                });
                break;
            }
        }

        // All is correct, let's continue
        next();
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;