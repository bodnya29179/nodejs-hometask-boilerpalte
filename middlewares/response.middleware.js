const responseMiddleware = (req, res, next) => {
   if (req && (req.method === 'GET' || req.method === 'DELETE')) {
      next();
   } else if (req && Object.keys(req.body).length) {
      next();
   } else {
      res.status(400).send({
         "error": true,
         "message": "Bad request!"
      });
   }
}

exports.responseMiddleware = responseMiddleware;