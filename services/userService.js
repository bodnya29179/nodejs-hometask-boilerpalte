const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // Get all users
    getAllUsers() {
        const users = UserRepository.getAll();
        return users ? users : null;
    }

    // Get user by ID
    getUserById(id) {
        return this.search({ id });
    }

    // Create a new user
    createUser(user) {
        return UserRepository.create(user);
    }

    // Update user's data
    updateUser(user) {
        const { id, ...dataToUpdate } = user;
        return UserRepository.update(id, dataToUpdate);
    }

    // Delete user
    deleteUser(id) {
        return UserRepository.delete(id);
    }

    // Find user
    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();