const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
        
    // Get all fighters
    getAllFighters() {
        const fighters = FighterRepository.getAll();
        return fighters ? fighters : null;
    }
    
    // Get fighter by ID
    getFighterById(id) {
        return this.search({ id });
    }
    
    // Create a new fighter
    createFighter(fighter) {
        return FighterRepository.create(fighter);
    }
    
    // Update fighters's data
    updateFighter(fighter) {
        const { id, ...dataToUpdate } = fighter;
        return FighterRepository.update(id, dataToUpdate);
    }
    
    // Delete fighter
    deleteFighter(id) {
        return FighterRepository.delete(id);
    }

    // Find user
    search(search) {
        const item = FighterRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();